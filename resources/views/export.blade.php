<h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>

@if($conteos->countAdm != 0)
    <h5>Ordenes por admitir: {{$conteos->countAdm}}</h5>
@endif

@if($conteos->countRea != 0)
    <h5>Ordenes por realizar: {{$conteos->countRea}}</h5>
@endif

@if($conteos->countDict != 0)
    <h5>Ordenes por dictar: {{$conteos->countDict}}</h5>
@endif

@if($conteos->countTrans != 0)
    <h5>Ordenes por transcribir: {{$conteos->countTrans}}</h5>
@endif

@if($conteos->countApr != 0)
    <h5>Ordenes por aprobar: {{$conteos->countApr}}</h5>
@endif

@if($conteos->countFin != 0)
    <h5>Ordenes finalizadas: {{$conteos->countFin}}</h5>
@endif

@if($conteos->countSus != 0)
    <h5>Ordenes suspendidas: {{$conteos->countSus}}</h5>
@endif

<h5>Ordenes totales: {{$conteos->countAdm + $conteos->countRea + $conteos->countDict + $conteos->countTrans + $conteos->countApr + $conteos->countFin + $conteos->countSus}}</h5>
<br>
<br>
<table>
    <thead>
        <tr>
            <th style='text-align:center; font-weight: bold;'>Localidad</th>
            <th style='text-align:center; font-weight: bold;'>Número de orden</th>
            <th style='text-align:center; font-weight: bold;'>Cédula</th>
            <th style='text-align:center; font-weight: bold;'>Procedimiento/Estudio</th>    
            <th style='text-align:center; font-weight: bold;'>Estatus</th>   
            <th style='text-align:center; font-weight: bold;'>Fecha culminado</th>   
            <th style='text-align:center; font-weight: bold;'>Pacs</th>
            <th style='text-align:center; font-weight: bold;'>N° imágenes pacs</th>
            <th style='text-align:center; font-weight: bold;'>Informe en viewmed</th>
            <th style='text-align:center; font-weight: bold;'>N° imágenes viewmed</th>
            <th style='text-align:center; font-weight: bold;'>Diferencia imágenes</th>
            <th style='text-align:center; font-weight: bold;'>Proyecciones adicionales</th>
            <th style='text-align:center; font-weight: bold;'>Factura</th>
            <th style='text-align:center; font-weight: bold;'>Fecha factura</th>
            <th style='text-align:center; font-weight: bold;'>Monto total</th>
            <th style='text-align:center; font-weight: bold;'>Porcentaje Viewmed</th>
         </tr>
    </thead>
    <tbody>
        @foreach($result as $info)  
            <tr>
                <td style='text-align:center'>{{$localidad}}</td>
                <td style='text-align:center'>{{$info->id}}</td>
                <td style='text-align:center'>{{$info->patient_identification_id}}</td>
                <td style='text-align:center'>{{$info->description}} ({{$info->modalidad}})</td>
                <td style='text-align:center'>{{$info->status}}</td>
                <td style='text-align:center'>{{$info->culmination_date}}</td>
                <td style='text-align:center'>
                    @if($info->pacs == true)
                        Si
                    @else
                        No
                    @endif
                </td>
                <td style='text-align:center'>{{$info->nimagenespacs}}</td>
                <td style='text-align:center'>{{$info->informe}}</td>
                <td style='text-align:center'>{{$info->nimagenesviewmed}}</td>
                <td style='text-align:center'>{{$info->nimagenespacs - $info->nimagenesviewmed}}</td>
                <td style='text-align:center'>{{$info->proyecciones_adicionales}}</td>
                <td style='text-align:center'>{{$info->numero_factura}} ({{$info->estatus_factura}})</td>
                <td style='text-align:center'>
                    @if($info->fecha_factura != 0)
                        {{date('d/m/Y', strtotime($info->fecha_factura))}}
                    @else
                        0
                    @endif
                </td>
                <td style='text-align:center'>{{$info->total}}</td>
                <td style='text-align:center'>{{$info->porcentaje_viewmed}}</td>
            </tr>
       @endforeach
    </tbody>
</table>