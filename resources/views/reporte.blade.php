<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Reporte Facturación</title>

    </head>
    <body>
        <script type="text/php">
            $size = 8;
            $y = 25;
            $x = $pdf->get_width() - 80;
            $font = $fontMetrics->get_font("sans-serif");
            $pdf->page_text($x, $y, " Página {PAGE_NUM}/{PAGE_COUNT}", $font, $size);
        </script>
        <main>

            <h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>

            @if($conteos->countAdm != 0)
                <h5>Ordenes por admitir: {{$conteos->countAdm}}</h5>
            @endif

            @if($conteos->countRea != 0)
                <h5>Ordenes por realizar: {{$conteos->countRea}}</h5>
            @endif

            @if($conteos->countDict != 0)
                <h5>Ordenes por dictar: {{$conteos->countDict}}</h5>
            @endif

            @if($conteos->countTrans != 0)
                <h5>Ordenes por transcribir: {{$conteos->countTrans}}</h5>
            @endif

            @if($conteos->countApr != 0)
                <h5>Ordenes por aprobar: {{$conteos->countApr}}</h5>
            @endif

            @if($conteos->countFin != 0)
                <h5>Ordenes finalizadas: {{$conteos->countFin}}</h5>
            @endif

            @if($conteos->countSus != 0)
                <h5>Ordenes suspendidas: {{$conteos->countSus}}</h5>
            @endif

            <h5>Ordenes totales: {{$conteos->countAdm + $conteos->countRea + $conteos->countDict + $conteos->countTrans + $conteos->countApr + $conteos->countFin + $conteos->countSus}}</h5>

            
            <table border = 1 cellspacing = 0 cellpadding = 0 style="padding: 5px;">
                <thead>
                    <tr>
                        <th>Localidad</th>
                        <th>Número de orden</th>
                        <th>Cédula</th>
                        <th>Procedimiento/Estudio</th>    
                        <th>Estatus</th>   
                        <th>Fecha culminado</th>   
                        <th>Pacs</th>
                        <th>Informe en viewmed</th>
                        <th>Factura</th>
                        <th>Fecha factura</th>
                        <th>Monto total</th>
                        <th>Porcentaje Viewmed</th>
                     </tr>
                </thead>
                <tbody>
                    @foreach($result as $info)  
                        <tr>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$localidad}}</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->id}}</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->patient_identification_id}}</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->description}} ({{$info->modalidad}})</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->status}}</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->culmination_date}}</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">
                                @if($info->pacs == true)
                                    Si
                                @else
                                    No
                                @endif
                            </td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->informe}}</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->numero_factura}} ({{$info->estatus_factura}})</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">
                                @if($info->fecha_factura != 0)
                                    {{date('d/m/Y', strtotime($info->fecha_factura))}}
                                @else
                                    0
                                @endif
                            </td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->total}}</td>
                            <td style='text-align:center' style="padding: 0px 5px; font-size: 10px;">{{$info->porcentaje_viewmed}}</td>
                        </tr>
                   @endforeach
                </tbody>
            </table>
        </main>
    </body>
</html>