<h4>Reporte facturacion fines de semana y feriados</h4>
<h4>desde {{$start_date}} al {{$end_date}}</h4>
<br>
<br>
<table>
    <thead>
        <tr>
        	<th style='text-align:center; font-weight: bold;'>IDACAS</th>
        	@php
			    $count = 1;
			@endphp
        	@foreach($result as $info)
        		@if($count == 1)
        			@foreach($info->periodo as $a)
	        			<th style='text-align:center; font-weight: bold;'>{{$a->periodo_formateado}}</th>
       				@endforeach
       				@php
			    		$count++;
					@endphp
       			@endif
       		@endforeach
     	</tr>
    </thead>
    <tbody>
        @foreach($result as $info)
            <tr>
                <td style='text-align:center'>{{$info->nombre_localidad}}</td>
                @foreach($info->periodo as $a)
                	<td style='text-align:center'>{{$a->cantidad_estudios}}</td>
       			@endforeach
            </tr>
       @endforeach
    </tbody>
</table>