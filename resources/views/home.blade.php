@extends('layouts.app')
@section('content')
    
    <!-- BACKDROP -->
    <div id="backdrop" class="fondo-opaco backdrop"></div>
    <!-- BACKDROP -->

    <div class="container">

        @if(Session::has('warning'))
            <div class="alert alert-warning alert-dismissible fade show text-white">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session::get('warning')}}
            </div>
        @endif

        <!-- PILLS -->
        <ul class="nav nav-pills mb-3 mb-2-personalizada" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active new-btn-primary tab-a" id="pills-viewmed-tab" data-toggle="pill" href="#pills-viewmed" role="tab" aria-controls="pills-viewmed" aria-selected="true">Viewmed</a>
            </li>
            <li class="nav-item tab-secondary" role="presentation">
                <a class="nav-link new-btn-primary tab-a tab-1" id="pills-finessemana-tab" data-toggle="pill" href="#pills-finessemana" role="tab" aria-controls="pills-finessemana" aria-selected="false">Fines de semana y feriados</a>
            </li>
        </ul>
        <!-- PILLS -->

        <div class="tab-content" id="pills-tabContent">

            <!-- PILL VIEWMED -->
            <div class="tab-pane fade show active tab-personalizada" id="pills-viewmed" role="tabpanel" aria-labelledby="pills-viewmed-tab">
                <form id="form-search" method="POST" action="{{ url('reporteviewmed') }}" role="form" class="needs-validation" novalidate>
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="panel-title">Facturación Viewmed</h3>
                        </div>
                        <div class="card-body">
                           <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <label for="localidad">Localidad</label>
                                    <select class="form-control" id="localidad_id" name="localidad_id" required>
                                        <option value = "04">Las Ciencias</option>
                                        <option value = "11">CMDLT</option>
                                        <option value = "12">CMDLT - MNU</option>
                                        <option value = "16">Oasis</option>
                                        <option value = "19">Sanatrix</option>
                                        <option value = "20">La Floresta</option>
                                        <option value = "21">Fenix Salud</option>
                                        <option value = "22">La Arboleda</option>
                                        <option value = "23">Higuerote</option>
                                        <option value = "25">La Viña</option>
                                    </select>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="start_date">Fecha inicio</label>
                                        <input type="date" name="start_date" id="start_date" class="form-control input-sm" required>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="end_date">Fecha fin</label>
                                        <input type="date" name="end_date" id="end_date" class="form-control input-sm" required>
                                    </div>
                                </div>
                            </div>
                         </div>
                        <div class="card-footer text-muted text-center">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="submit"  value="Generar" class="btn btn-primary btn-generar">
                            </div>
                        </div>
                    </div>
                </form>
                <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="height: 200px;">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" id="toastP" style="max-width: 400px !important;">
                        <div class="toast-header" style="color: #000 !important; background-color: rgba(0,0,0,.03);">
                            <strong class="mr-auto">Aviso</strong>
                             <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                            Generando el reporte, esto puede demorar varios minutos. Cuando finalice el reporte estará en "Descargas"...
                        </div>
                    </div>
                </div>
            </div>
            <!-- PILL VIEWMED -->


            <!-- PILL FINES DE SEMANA Y FERIADOS -->
            <div class="tab-pane fade tab-personalizada" id="pills-finessemana" role="tabpanel" aria-labelledby="pills-finessemana-tab">
                <form id="formoid" method="POST" action="{{ url('reportefinessemanaferiados') }}" role="form" class="needs-validation" novalidate>
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="panel-title">Facturación fines de semana y feriados</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="start_date">Fecha inicio</label>
                                        <input type="date" name="start_date1" id="start_date1" class="form-control input-sm" required>
                                        <div class="invalid-feedback">
                                            Debe llenar este campo
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="end_date">Fecha fin</label>
                                        <input type="date" name="end_date1" id="end_date1" class="form-control input-sm" required>
                                        <div class="invalid-feedback">
                                            Debe llenar este campo
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-muted text-center">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="submit"  value="Generar" class="btn btn-primary new-btn-primary btn-generar">
                            </div>
                        </div>
                    </div>
                </form>
                <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="height: 200px;">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" id="toastP" style="max-width: 400px !important;">
                        <div class="toast-header" style="color: #000 !important; background-color: rgba(0,0,0,.03);">
                            <strong class="mr-auto">Aviso</strong>
                             <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                            Generando el reporte, esto puede demorar varios minutos. Cuando finalice el reporte estará en "Descargas"...
                        </div>
                    </div>
                </div>
            </div>
            <!-- PILL FINES DE SEMANA Y FERIADOS -->
        </div>
    </div>

    <!-- ALERT FECHA INICIO -->
    <div class="alert alert-warning alert-dismissible fade show alert-personalizada alert-warning" id="fecha_inicio">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Fecha de inicio no puede ser mayor a la fecha de fin...
    </div>
    <!-- ALERT FECHA INICIO -->

    <!-- ALERT FECHA FIN -->
    <div class="alert alert-warning alert-dismissible fade show alert-personalizada alert-warning" id="fecha_fin">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Fecha de fin no puede ser menor a la fecha de inicio...
    </div>
    <!-- ALERT FECHA FIN -->

    <script>
        $(document).ready(function(){

            // hiden the alert
            setTimeout(function(){
                $(".alert").alert('close');
            }, 3000);

            // Setear fecha a los inputs
            $('.tab-1').click(function(e){
                setFechas("start_date1", "end_date1");
            });

            setFechas("start_date", "end_date");

            function setFechas(inicio, fin){
                today = formatFecha();

                document.getElementById(inicio).value = today;
                document.getElementById(fin).value = today;
                $("#"+inicio, "#"+fin).attr({
                    "max" : today
                });
            }

            function formatFecha(){
               
                var date = new Date()
                var day = date.getDate()
                var month = date.getMonth() + 1
                var year = date.getFullYear()

                month = month < 10 ? '0' + month : month
                day = day < 10 ? '0' + day : day

                return year+'-'+month+'-'+day
            }

            $("#start_date").change(function(){
                validarFechas("start_date", "end_date", "#fecha_inicio", "inicio");
            });


            $("#end_date").change(function(){
                validarFechas("end_date", "start_date", "#fecha_fin", "fin");
            });

            $("#start_date1").change(function(){
                validarFechas("start_date1", "end_date1", "#fecha_inicio", "inicio");
            });


            $("#end_date1").change(function(){
                validarFechas("end_date1", "start_date1", "#fecha_fin", "fin");
            });


            $(".btn-generar").click(function(){
                $('#toastP').toast('show')
                $("#backdrop").css("display", "block");
                setTimeout(function(){
                    $("#backdrop").css("display", "none");
                    $('#toastP').toast('hide')
                }, 10000);
            });


            function validarFechas(inicio, fin, div, tipo){
                if(tipo == 'inicio'){
                    if($("#"+inicio).val() > $("#"+fin).val()){
                        
                        $(div).css("display", "block");
                        setTimeout(function(){
                            $(div).css("display", "none");
                        }, 2000);

                        document.getElementById(inicio).value = $("#"+fin).val();
                    }
                }else{
                    if($("#"+inicio).val() < $("#"+fin).val()){
                        
                        $(div).css("display", "block");
                        setTimeout(function(){
                            $(div).css("display", "none");
                        }, 2000);

                        document.getElementById(inicio).value = $("#"+fin).val();
                    }
                }
            }

        });

    </script>

@endsection
