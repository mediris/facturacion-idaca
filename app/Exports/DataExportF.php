<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class DataExportF implements FromView, ShouldAutoSize{

    use Exportable;

    public function __construct($data, $start_date, $end_date){
        $this->data = $data;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function view(): View{
        return view('exportF', [
            'result' => $this->data,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date
        ]);
    }

}