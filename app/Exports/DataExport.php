<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class DataExport implements FromView, ShouldAutoSize{

    use Exportable;

    public function __construct($data, $conteos, $start_date, $end_date, $localidad){
        $this->data = $data;
        $this->conteos = $conteos;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->localidad = $localidad;
    }

    public function view(): View{
        return view('export', [
            'result' => $this->data, 
            'conteos' => $this->conteos,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'localidad' => $this->localidad
        ]);
    }

}