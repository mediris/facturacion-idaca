<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

use App\Exports\DataExport;
use App\Exports\DataExportF;


class HomeController extends Controller{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('home');
    }


    public function reportViewmed(Request $request){

        $start_date = str_replace("-", "_", $request->all()['start_date']);
        $end_date = str_replace("-", "_", $request->all()['end_date']);

        $url = "http://192.168.2.210/webservice-mediris/API/facturacion/".$request->all()['localidad_id']."-".$start_date."-".$end_date;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);

        if($result == 'No hay reportes con los criterios seleccionados'){
            return redirect()->route('home')->with('warning','No se encontró información con los parametros de busqueda');
        }else{

            ini_set('memory_limit', -1);
            ini_set('max_execution_time', 0);
            set_time_limit(0);

            // convertir string to array
            $result1 = json_decode($result);
            // convertir array to object
            $object = (object) $result1;

            $conteos = $this->conteos($object);
            
            $start_date = date("d/m/Y", strtotime($request->all()['start_date']));
            $end_date = date("d/m/Y", strtotime($request->all()['end_date']));

            $localidad = $this->getLocalidad($request->all()['localidad_id']);

            return (new DataExport($object, $conteos, $start_date, $end_date, $localidad))->download("Reporte Localidad " .$localidad. ".xlsx");
        }
    }

    public function reportFinesSemanaFeriados(Request $request){

        $start_date = str_replace("-", "_", $request->all()['start_date1']);
        $end_date = str_replace("-", "_", $request->all()['end_date1']);

        $url = "http://192.168.2.210/webservice-mediris/API/facturacionfsyf/".$start_date."-".$end_date;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);

        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        set_time_limit(0);

        // convertir string to array
        $result1 = json_decode($result);

        // convertir array to object
        $object = (object) $result1;

        $start_date = date("d/m/Y", strtotime($request->all()['start_date1']));
        $end_date = date("d/m/Y", strtotime($request->all()['end_date1']));

        return (new DataExportF($object, $start_date, $end_date))->download("Reporte fines de semana y feriados.xlsx");
    }


    public function conteos($object){
        
        $countAdm = 0;
        $countRea = 0;
        $countDict = 0;
        $countTrans = 0;
        $countApr = 0;
        $countFin = 0;
        $countSus = 0;

        foreach ($object as $key => $value){
            if($value->requested_procedure_status_id == '1'){
                $countAdm += 1;
            }elseif($value->requested_procedure_status_id == '2'){
                $countRea += 1;
            }elseif($value->requested_procedure_status_id == '3'){
                $countDict += 1;
            }elseif($value->requested_procedure_status_id == '4'){
                $countTrans += 1;
            }elseif($value->requested_procedure_status_id == '5'){
                $countApr += 1;
            }elseif($value->requested_procedure_status_id == '6'){
                $countFin += 1;
            }elseif($value->requested_procedure_status_id == '7'){
                $countSus += 1;
            }
        }

        return (object) array('countAdm' => $countAdm, 'countRea' => $countRea, 'countDict' => $countDict, 'countTrans' => $countTrans, 'countApr' => $countApr, 'countFin' => $countFin, 'countSus' => $countSus);
    }


    public function getLocalidad($localidad){

        if($localidad == '04'){
            $name = 'Las Ciencias';
        }else if($localidad == '11'){
            $name = 'CMDLT';
        }else if($localidad == '12'){
            $name = 'CMDLT Medicina Nuclear';
        }else if($localidad == '16'){
            $name = 'Oasis';
        }else if($localidad == '19'){
            $name = 'Sanatrix';
        }else if($localidad == '20'){
            $name = 'La Floresta';
        }else if($localidad == '21'){
            $name = 'Fénix';
        }else if($localidad == '22'){
            $name = 'La Arboleda';
        }else if($localidad == '23'){
            $name = 'Higuerote';
        }

        return $name;
    }

}
